import random
from pathlib import Path
from urllib.parse import urljoin

import requests
import yaml
from faker import Faker

CURRENT_DIR = Path(__file__).resolve().parent
DEFAULT_CONFIG = {
    "number_of_users": 0,
    "max_posts_per_user": 0,
    "max_likes_per_user": 0,
}


def validate_config(config: dict):
    for value in config.values():
        if not isinstance(value, int) and value >= 0:
            raise Exception("All setting values must be int types and >= 0.")


with open(Path.joinpath(CURRENT_DIR, "config.yml"), "r") as file:
    parsed_config = yaml.safe_load(file)

validate_config(parsed_config)
config = DEFAULT_CONFIG | parsed_config


class ApiClient:
    def __init__(self):
        self.base_url = "http://127.0.0.1:8000/api/v1/"
        self.fake = Faker()

    def _request(self, method, url, raise_exception=True, **kwargs):
        response = requests.request(method=method, url=urljoin(self.base_url, url), **kwargs)
        if raise_exception and not response.ok:
            print(response.content)
            raise Exception("Service unavailable")
        return response

    def signup_user(self):
        success, data = False, {}
        while not success:
            data = {"username": self.fake.first_name(), "password": self.fake.password()}
            response = self._request(
                method="post", url="authentication/signup/", raise_exception=False, data=data
            )
            # Retry if the error was due to a non-unique username
            if response.ok or response.json().get("username", None) != [
                "A user with that username already exists."
            ]:
                success = True
        return data

    def login(self, username, password):
        data = {"username": username, "password": password}
        response = self._request(method="post", url="authentication/login/", data=data)
        return response.json()

    def create_post(self, access_token):
        data = {"message": self.fake.text()}
        response = self._request(
            method="post",
            url="post/",
            data=data,
            headers={"AUTHORIZATION": f"Bearer {access_token}"},
        )
        return response.json()

    def like_post(self, access_token, post_id):
        self._request(
            method="post",
            url=f"post/{post_id}/like/",
            headers={"AUTHORIZATION": f"Bearer {access_token}"},
        )


client = ApiClient()

# Signup users
user_tokens = []
for _ in range(config["number_of_users"]):
    user_info = client.signup_user()
    tokens = client.login(**user_info)
    user_tokens.append(tokens["access"])


# Create posts
post_ids = []
for token in user_tokens:
    for _ in range(random.randint(0, config["max_posts_per_user"])):
        post = client.create_post(access_token=token)
        post_ids.append(post["id"])

# Like posts
posts_amount = len(post_ids)
for token in user_tokens:
    random.shuffle(post_ids)
    for idx in range(random.randint(0, min(config["max_likes_per_user"], posts_amount))):
        client.like_post(access_token=token, post_id=post_ids[idx])

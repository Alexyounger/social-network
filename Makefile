clean:
	black .; isort .; flake8 .;

run:
	./manage.py runserver

db:
	./manage.py migrate

test:
	./manage.py test

run_bot:
	python3 bot/entry.py


from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin


class UserLastRequestMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        user = getattr(request, "user", None)
        if user and not user.is_anonymous:
            user.last_request = timezone.now()
            user.save(update_fields=("last_request",))
        return response

from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from apps.authentication.views import SignupView, UserActivityView

router = routers.DefaultRouter()
router.register("user", UserActivityView, basename="user")

urlpatterns = [
    path("authentication/signup/", SignupView.as_view(), name="signup"),
    path("authentication/login/", TokenObtainPairView.as_view(), name="login"),
    path("authentication/refresh-token/", TokenRefreshView.as_view(), name="refresh-token"),
] + router.urls

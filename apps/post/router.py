from rest_framework import routers

from apps.post.views import LikeViewSet, PostViewSet

router = routers.DefaultRouter()
router.register("post", PostViewSet, basename="post")
router.register("like", LikeViewSet, basename="like")

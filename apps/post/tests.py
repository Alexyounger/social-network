from datetime import datetime
from unittest import mock

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from apps.authentication.models import User
from apps.authentication.tests import get_tokens
from apps.post.exceptions import AlreadyLikedException
from apps.post.models import Like, Post


class PostViewSetTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user("John", password="Qwerty@123")
        access, _ = get_tokens(user=self.user)
        self.auth_header = {"HTTP_AUTHORIZATION": f"Bearer {access}"}

    def test_create(self):
        url = reverse("post-list")
        data = {"message": "Hello World!"}

        # Unauthorized
        r = self.client.post(url, data)
        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

        # Success
        r = self.client.post(url, data, **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        self.assertEqual(r.data["message"], data["message"])

    def test_detail(self):
        post = Post.objects.create(author=self.user, message="Hello World!")
        user_1 = User.objects.create_user("Sam", password="Qwerty@123")
        user_2 = User.objects.create_user("Dean", password="Qwerty@123")
        Like.objects.create(user=user_1, post=post)
        Like.objects.create(user=user_2, post=post)

        r = self.client.get(reverse("post-detail", args=(post.id,)), **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["message"], "Hello World!")
        self.assertEqual(r.data["likes_amount"], 2)
        self.assertEqual(r.data["author_id"], self.user.id)

    def test_like(self):
        post = Post.objects.create(author=self.user, message="Hello World!")

        # Success
        r = self.client.post(reverse("post-like", args=(post.id,)), **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        # Post has already liked by this user
        r = self.client.post(reverse("post-like", args=(post.id,)), **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(r.data["detail"], AlreadyLikedException.default_detail)

    def test_unlike(self):
        post = Post.objects.create(author=self.user, message="Hello World!")
        Like.objects.create(user=self.user, post=post)

        self.assertEqual(post.likes.count(), 1)
        r = self.client.post(reverse("post-unlike", args=(post.id,)), **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(post.likes.count(), 0)


class LikeViewSetTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user("John", password="Qwerty@123")
        access, _ = get_tokens(user=self.user)
        self.auth_header = {"HTTP_AUTHORIZATION": f"Bearer {access}"}

    def test_analytics(self):
        url = reverse("like-analytics")
        post_1 = Post.objects.create(author=self.user, message="Hello World!")
        post_2 = Post.objects.create(author=self.user, message="Bye World!")

        user_1 = User.objects.create_user("Sam", password="Qwerty@123")

        with mock.patch("django.utils.timezone.now", return_value=datetime(2023, 2, 7)):
            Like.objects.create(user=self.user, post=post_1)

        with mock.patch("django.utils.timezone.now", return_value=datetime(2023, 2, 8)):
            Like.objects.create(user=user_1, post=post_1)
            Like.objects.create(user=user_1, post=post_2)
            Like.objects.create(user=self.user, post=post_2)

        # Without timerange
        r = self.client.get(url, **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data, {"2023-02-07": 1, "2023-02-08": 3})

        # With timerange
        r = self.client.get(url + "?date_from=2023-02-06&date_to=2023-02-09", **self.auth_header)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(
            r.data, {"2023-02-06": 0, "2023-02-07": 1, "2023-02-08": 3, "2023-02-09": 0}
        )

        # Test filter
        r = self.client.get(url + "?date_from=2023-02-06&date_to=2023-02-07", **self.auth_header)
        self.assertEqual(r.data, {"2023-02-06": 0, "2023-02-07": 1})

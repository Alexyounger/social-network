from django_filters import rest_framework as filters

from apps.post.models import Like


class LikeFilterSet(filters.FilterSet):
    date_from = filters.DateFilter(method="filter_date_from")
    date_to = filters.DateFilter(method="filter_date_to")

    class Meta:
        model = Like
        fields = ("post_id",)

    def filter_date_from(self, queryset, name, value):
        return queryset.filter(created_at__date__gte=value)

    def filter_date_to(self, queryset, name, value):
        return queryset.filter(created_at__date__lte=value)

from rest_framework import status
from rest_framework.exceptions import APIException


class AlreadyLikedException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "The post has already been marked as liked."

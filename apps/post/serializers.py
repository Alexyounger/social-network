from rest_framework import serializers

from apps.post.models import Like, Post


class PostSerializer(serializers.ModelSerializer):
    likes_amount = serializers.SerializerMethodField()

    class Meta:
        model = Post
        read_only_fields = ("id", "author_id", "likes_amount", "created_at", "updated_at")
        fields = ("id", "message", "author_id", "likes_amount", "created_at", "updated_at")

    def get_likes_amount(self, post):
        return post.likes.count()

    def create(self, validated_data):
        validated_data.update(author=self.context["request"].user)
        return super().create(validated_data=validated_data)


class DateRangeSerializer(serializers.Serializer):
    date_from = serializers.DateField(required=False, write_only=True)
    date_to = serializers.DateField(required=False, write_only=True)


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ("post_id", "user_id", "created_at")

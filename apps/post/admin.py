from django.contrib import admin

from apps.post.models import Like, Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    model = Post
    list_select_related = ("author",)
    list_display = ("author", "message", "created_at", "updated_at")


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    model = Like
    list_select_related = ("user", "post")
    raw_id_fields = ("post", "user")
    list_display = ("user", "post_id", "created_at")
